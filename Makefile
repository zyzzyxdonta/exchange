CC     = cc
CFLAGS = -Wall -Wextra -O3

PROGRAM     = exchange
DESTINATION = /usr/local/bin/

$(PROGRAM): %: %.c
	$(CC) $(CFLAGS) $^ -o $@

.PHONY: clean install

clean:
	rm -f $(PROGRAM)

install: $(PROGRAM)
	install -s $(PROGRAM) $(DESTINATION)
